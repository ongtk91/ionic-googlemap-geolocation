import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LeaveManagementPage } from '../pages/leave-management/leave-management';
import { LoginPage } from '../pages/login/login';
import { ReportingPage } from '../pages/reporting/reporting';
import { SettingPage } from '../pages/setting/setting';
import { SideMenuPage } from '../pages/sideMenu/sideMenu';

import { AuthenticationHandler } from '../providers/authenticationHandler/authenticationHandler';
import { CallApiHandler } from '../providers/callApiHandler/callApiHandler';

import { Geolocation } from '@ionic-native/geolocation';
import { NgxQRCodeModule } from 'ngx-qrcode2';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LeaveManagementPage,
    LoginPage,
    ReportingPage,
    SettingPage,
    SideMenuPage
  ],
  imports: [
    NgxQRCodeModule,
    BrowserModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LeaveManagementPage,
    LoginPage,
    ReportingPage,
    SettingPage,
    SideMenuPage
  ],
  providers: [
    AuthenticationHandler,
    CallApiHandler,
    Geolocation,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

import { Component } from '@angular/core';
import { NavController, AlertController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ReportingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-reporting',
  templateUrl: 'reporting.html',
})
export class ReportingPage {

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportingPage');
  }

  goAttendanceRecord() {
    let comingSoonAlert = this.alertCtrl.create({
      title: 'Feature Coming Soon',
      subTitle: 'Thank you for choosing ClockIn!',
      buttons: ['OK']
    });
    comingSoonAlert.present();
  }

  goTodayAbsence() {
    let comingSoonAlert = this.alertCtrl.create({
      title: 'Feature Coming Soon',
      subTitle: 'Thank you for choosing ClockIn!',
      buttons: ['OK']
    });
    comingSoonAlert.present();
  }

  goTodayQrClockIn() {
    let comingSoonAlert = this.alertCtrl.create({
      title: 'Feature Coming Soon',
      subTitle: 'Thank you for choosing ClockIn!',
      buttons: ['OK']
    });
    comingSoonAlert.present();
  }

  goTodayGpsClockIn() {
    let comingSoonAlert = this.alertCtrl.create({
      title: 'Feature Coming Soon',
      subTitle: 'Thank you for choosing ClockIn!',
      buttons: ['OK']
    });
    comingSoonAlert.present();
  }
}

import { Component } from '@angular/core';
import { NavController, MenuController, Events } from 'ionic-angular';

import { HomePage } from '../home/home';
import { LeaveManagementPage } from '../leave-management/leave-management';
import { LoginPage } from '../login/login';
import { ReportingPage } from '../reporting/reporting';
import { SettingPage } from '../setting/setting';

import { AuthenticationHandler } from '../../providers/authenticationHandler/authenticationHandler';

@Component({
  selector: 'page-sideMenu',
  templateUrl: 'sideMenu.html'
})
export class SideMenuPage {
  private rootPage;
  private homePage;
  private leaveManagementPage;
  private reportingPage;
  private settingPage;
  private loginPage;

  isSignIn: boolean = false;
  username: String = "NA";

  constructor(public navCtrl: NavController, public menuController: MenuController, public events: Events, public authenticationHandler: AuthenticationHandler) {
    this.rootPage = HomePage;
    
    this.homePage = HomePage;
    this.leaveManagementPage = LeaveManagementPage;
    this.reportingPage = ReportingPage;
    this.settingPage = SettingPage;
    this.loginPage = LoginPage;

    authenticationHandler.authenticateToken().then( result => {
      if(result == true) {
        authenticationHandler.retrieveToken().then( username => {
          this.isSignIn = result;

          this.username = username;
        });
      }
    });

    events.subscribe('user:signIn', (user, time) => {
      this.authenticationHandler.retrieveToken().then( result => {
        this.username = result;
        this.isSignIn = true;
        this.rootPage = this.homePage;
      });
    });
  }

  openPage(target) {
    this.rootPage = target;
    this.menuController.close();
  }

  signOut() {
    this.authenticationHandler.removeToken().then(result => {
      if(result == true) {
        this.isSignIn = false;
        this.menuController.close();
        this.rootPage = this.homePage;
      }
    });
  }
}

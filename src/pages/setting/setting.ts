import { Component } from '@angular/core';
import { NavController, AlertController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingPage');
  }

  goNotification() {
    let comingSoonAlert = this.alertCtrl.create({
			title: 'Feature Coming Soon',
			subTitle: 'Thank you for choosing ClockIn!',
			buttons: ['OK']
		});
		comingSoonAlert.present();
  }

  goUserDetails() {
    let comingSoonAlert = this.alertCtrl.create({
			title: 'Feature Coming Soon',
			subTitle: 'Thank you for choosing ClockIn!',
			buttons: ['OK']
		});
		comingSoonAlert.present();
  }

  goUserGuides() {
    let comingSoonAlert = this.alertCtrl.create({
			title: 'Feature Coming Soon',
			subTitle: 'Thank you for choosing ClockIn!',
			buttons: ['OK']
		});
		comingSoonAlert.present();
  }

  goSupportDetails() {
    let comingSoonAlert = this.alertCtrl.create({
			title: 'Feature Coming Soon',
			subTitle: 'Thank you for choosing ClockIn!',
			buttons: ['OK']
		});
		comingSoonAlert.present();
  }
}

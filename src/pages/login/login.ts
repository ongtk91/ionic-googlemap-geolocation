import { Component } from '@angular/core';
import { NavController, AlertController, NavParams, Events } from 'ionic-angular';

import { AuthenticationHandler } from '../../providers/authenticationHandler/authenticationHandler';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams, public events: Events, public authenticationHandler: AuthenticationHandler) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  signUp() {
    let comingSoonAlert = this.alertCtrl.create({
			title: 'Feature Coming Soon',
			subTitle: 'Thank you for choosing ClockIn!',
			buttons: ['OK']
		});
		comingSoonAlert.present();
  }

  forgotPassword() {
    let comingSoonAlert = this.alertCtrl.create({
			title: 'Feature Coming Soon',
			subTitle: 'Thank you for choosing ClockIn!',
			buttons: ['OK']
		});
		comingSoonAlert.present();
  }

  signIn() {
    /*let comingSoonAlert = this.alertCtrl.create({
			title: 'Feature Coming Soon',
			subTitle: 'Thank you for choosing ClockIn!',
			buttons: ['OK']
		});
    comingSoonAlert.present();*/
    this.authenticationHandler.injectToken('admin', 'NA').then( result => {
      this.events.publish('user:signIn', null, Date.now());
    });
  }
}

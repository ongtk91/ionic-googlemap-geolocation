import { Component } from '@angular/core';
import { NavController, AlertController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LeaveManagementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-leave-management',
  templateUrl: 'leave-management.html',
})
export class LeaveManagementPage {

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LeaveManagementPage');
  }

  goLeaveApplication() {
    let comingSoonAlert = this.alertCtrl.create({
			title: 'Feature Coming Soon',
			subTitle: 'Thank you for choosing ClockIn!',
			buttons: ['OK']
		});
		comingSoonAlert.present();
  }

  goLeaveRecord() {
    let comingSoonAlert = this.alertCtrl.create({
			title: 'Feature Coming Soon',
			subTitle: 'Thank you for choosing ClockIn!',
			buttons: ['OK']
		});
		comingSoonAlert.present();
  }

  goLeaveBalance() {
    let comingSoonAlert = this.alertCtrl.create({
			title: 'Feature Coming Soon',
			subTitle: 'Thank you for choosing ClockIn!',
			buttons: ['OK']
		});
		comingSoonAlert.present();
  }

  goLeaveListing() {
    let comingSoonAlert = this.alertCtrl.create({
			title: 'Feature Coming Soon',
			subTitle: 'Thank you for choosing ClockIn!',
			buttons: ['OK']
		});
		comingSoonAlert.present();
  }
}

import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';

declare var google: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  location: any;

  type: any;

  qrData = "admin";
  qrImage: any;

  currentDateTime: any;
  clock: any;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public geoLocation: Geolocation) {
    this.type = 'qrClock';
  }

  ionViewWillEnter() {
    this.createQrImage();
  }

  ionViewDidLoad() {
    setInterval(() => {
      this.currentDateTime = new Date();
      this.clock = this.currentDateTime.toLocaleString().replace(',', '');
    }, 1000);
  }

  createQrImage() {
    this.qrImage = this.qrData;
  }

  initMap() {
    let loading = this.loadingCtrl.create({ content: "Please wait..." });
    loading.present().then(result => {
      var defaultLatitude = '3.15785';
      var defaultLongtitude = '101.71165';

      this.getLocation().then(res => {
        var location = new google.maps.LatLng(res.coords.latitude, res.coords.longitude);

        var options = {
          center: location,
          zoom: 15
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, options);

        this.addMarker(location, this.map);

        this.callGoogleGetAddress(res.coords.latitude, res.coords.longitude).then(result => {
          if (result == false) {
            let errorAlert = this.alertCtrl.create({
              title: 'Ops',
              subTitle: 'Please turn on your location and go outdoor!',
              buttons: ['OK']
            });
            errorAlert.present();
          } else {
            this.location = result;
          }
        });

        google.maps.event.addListenerOnce(this.map, 'idle', function () {
          loading.dismissAll();
        });
      }).catch(err => {
        var location = new google.maps.LatLng(defaultLatitude, defaultLongtitude);

        var options = {
          center: location,
          zoom: 15
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, options);

        google.maps.event.addListenerOnce(this.map, 'idle', function () {
          loading.dismissAll();
        });

        let errorAlert = this.alertCtrl.create({
          title: 'Ops',
          subTitle: 'Please turn on your location and go outdoor!',
          buttons: ['OK']
        });
        errorAlert.present();
      });

    });
  }

  callGoogleGetAddress(latitude, longitude): Promise<any> {
    return new Promise((resolve, reject) => {
      var geocoder = new google.maps.Geocoder;
      var latlng = { lat: latitude, lng: longitude };
      geocoder.geocode({ 'location': latlng }, function (results, status) {
        if (status === 'OK') {
          if (results[0]) {
            resolve(results[0].formatted_address);
          } else {
            resolve(false);
          }
        } else {
          resolve(false);
        }
      });
    });
  }

  getLocation() {
    return this.geoLocation.getCurrentPosition();
  }

  addMarker(position, map) {
    return new google.maps.Marker({
      position,
      map
    });
  }

  qrClockIn() {
    let comingSoonAlert = this.alertCtrl.create({
      title: 'Feature Coming Soon',
      subTitle: 'Thank you for choosing ClockIn!',
      buttons: ['OK']
    });
    comingSoonAlert.present();
  }

  gpsClockIn() {
    let comingSoonAlert = this.alertCtrl.create({
      title: 'Feature Coming Soon',
      subTitle: 'Thank you for choosing ClockIn!',
      buttons: ['OK']
    });
    comingSoonAlert.present();
  }
}

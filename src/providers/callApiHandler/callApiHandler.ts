import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { AuthenticationHandler } from "../../providers/authenticationHandler/authenticationHandler";

@Injectable()
export class CallApiHandler {
	apiRootUrl = "https://api.motomart.my/";

	constructor(public http: Http, public authenticationHandler: AuthenticationHandler) {
		
	}
}
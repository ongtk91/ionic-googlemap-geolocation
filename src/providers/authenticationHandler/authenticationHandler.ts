import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class AuthenticationHandler {
	constructor(public storage: Storage) {

	}

	authenticateToken(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.storage.get('com.sim.clockin.jwt').then(token => {
				if (token == null) {
					resolve(false);
				} else if (token == '') {
					resolve(false);
				} else {
					resolve(true);
				}
			});
		});
	}

	injectToken(token, by): Promise<any> {
		return new Promise((resolve, reject) => {
			this.storage.set('com.sim.clockin.jwt', token).then(val => {
				this.storage.set('by', by).then(val => {
					resolve(true);
				});
			});
		});
	}

	retrieveToken(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.storage.get('com.sim.clockin.jwt').then(token => {
				resolve(token);
			});
		});
	}

	retrieveTokenInjectFrom(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.storage.get('by').then(by => {
				resolve(by);
			});
		});
	}

	removeToken(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.storage.remove('com.sim.clockin.jwt').then(val => {
				resolve(true);
			});
		});
	}
}